( function( $ ) {
    $('#emailBtn').on('click', function () {
        var inp = $('#inputEmail');
        var value = inp.val();

        $.ajax({
            beforeSend: (xhr) => xhr.setRequestHeader('X-WP-Nonce', jsData.nonce),
            type: 'POST',
            url: jsData.root_url + '/wp-json/subscribe_email/v1/email',
            data: {
                'email': value
            },
            dataType: 'text',
            success: function(){
                alert('You were successfully subscribed!');
                inp.val('');
            },
            error: function () {
                alert('OOPS! Something went wrong :(');
            }
        });
    });
} )( jQuery );
