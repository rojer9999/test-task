<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Demo_Starter_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" class="<?php echo 'card text-white bg-dark mb-3 ' . join(' ', get_post_class() ) ?>" style="max-width: 25rem;">
    <header class="card-header">
        <a href="<?php the_permalink(); ?>">
            <?php the_title( '<h5>', '</h5>' ); ?>
        </a>
    </header>
    <div class="card-body">
        <?php
        the_post_thumbnail();
        the_content();
        ?>
    </div>
    <footer class="card-body">
        <div class="row">
            <div class="col">
                <?php the_date('j / F / Y'); ?>
            </div>
            <div class="col text-right">
                <?php
                $first_name = get_the_author_meta('first_name');
                $last_name = get_the_author_meta('last_name');
                ?>
                <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
                    <?php echo "$first_name $last_name"; ?>
                </a>
            </div>
        </div>
    </footer>
</article>