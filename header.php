<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Demo_Starter_Theme
 */

//  import custom walker menu
require_once( __DIR__ . '/inc/walker-menu.php');
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'demo-starter' ); ?></a>

	<header id="masthead" class="site-header">
        <nav id="site-navigation" class="navbar navbar-expand-lg navbar-dark bg-dark main-navigation">
            <div class="container">
                <?php
                if (has_custom_logo()) {
                    the_custom_logo();
                } else {
                    ?> <a class="navbar-brand" href="#">I'm not a logo :/</a> <?php
                }
                ?>
                <button class="navbar-toggler menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
                $args = array(
                    'theme_location'    => 'menu-1',
                    'container_class'   => 'navbar-collapse',
                    'menu_class'        => 'navbar-nav mr-auto',
                    'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'menu_id'           => 'primary-menu',
                    'walker'            => new my_walker_nav_menu()
                );

                wp_nav_menu( $args );
                ?>
            </div>
        </nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
