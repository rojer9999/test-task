<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Demo_Starter_Theme
 */

get_header();
?>
    <div class="container">
        <div class="row">
            <div class="col-md">
                <h3>The newest posts</h3>
                <?php
                $the_query = new WP_Query(['posts_per_page' => 3]);
                while ($the_query -> have_posts()) {
                    $the_query -> the_post();
                    get_template_part( 'template-parts/content', 'newest' );
                }
                wp_reset_postdata();
                ?>
            </div>
            <div class="col-md">
                <h3>The most popular posts</h3>
                <?php
                $the_query = new WP_Query(array(
                    'posts_per_page' => 3,
                    'meta_query' => [
                        'post_views' => [
                            'key' => 'post_views_count',
                            'type' => 'NUMERIC'
                        ]
                    ],
                    'orderby' => 'post_views',
                    'order' => 'DESC'
                ));
                while ($the_query -> have_posts()) {
                    $the_query -> the_post();
                    get_template_part( 'template-parts/content', 'popular' );
                }
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2" id="inputEmail">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="emailBtn">Button</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_sidebar();
get_footer();
