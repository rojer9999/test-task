<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Demo_Starter_Theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="site-info">
                        <?php if( get_theme_mod( 'footer_text_block') != "" ): ?>
                            <p class="footer-text">
                                <?php echo get_theme_mod( 'footer_text_block'); ?>
                            </p>
                        <?php endif; ?>
                        <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'demo-starter' ) ); ?>">
                            <?php
                            /* translators: %s: CMS name, i.e. WordPress. */
                            printf( esc_html__( 'Proudly powered by %s', 'demo-starter' ), 'WordPress' );
                            ?>
                        </a>
                        <span class="sep"> | </span>
                        <?php
                        /* translators: 1: Theme name, 2: Theme author. */
                        printf( esc_html__( 'Theme: %1$s by %2$s.', 'demo-starter' ), 'demo-starter', '<a href="http://www.idonthaveone.com">Andrey Akimov</a>' );
                        ?>
                    </div><!-- .site-info -->
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
