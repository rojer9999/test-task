<?php

add_action('rest_api_init', 'emailRoute');
function emailRoute() {
    register_rest_route('subscribe_email/v1', 'email', array(
        'methods'  => 'POST',
        'callback' => 'save_email'
    ));
}

function save_email($data) {
    if (!is_user_logged_in()) {
        die("You're not logged in!");
    }

    $email = sanitize_text_field($data['email']);

    return wp_insert_post(array(
        'post_type'   => 'email',
        'post_status' => 'publish',
        'post_title'  => $email,
        'meta_input'  => array(
            'email'   => $email,
        ),
    ));
}